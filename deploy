#! /usr/bin/env nix-shell
#! nix-shell --pure -i runghc -p rsync openssh "haskellPackages.ghcWithPackages (pkgs: [ pkgs.turtle ])"
{-# LANGUAGE OverloadedStrings #-}
import Turtle
import Prelude hiding (FilePath)
import qualified Data.Text as T

sshOpts :: [Text]
sshOpts = ["-o", "StrictHostKeyChecking=no", "-i", "SECRET_private_key"]

rsync :: Text -> Text -> Shell Line
rsync host file =
  inproc "rsync"
    ["-rzP", "-e", "ssh " <> T.unwords sshOpts, file, "root@" <> host <> ":/root/"] empty

ssh :: Text -> Text -> Shell Line
ssh host cmd =
  inproc "ssh"
    (sshOpts <> ["root@"<>host, cmd]) empty

deploy :: Text -> Shell Line
deploy host =
  ssh host "nixos-rebuild build --show-trace && nixos-rebuild switch"

main = do
  host <- options "Deploy application" $ argText "host" "host to deploy to"
  stdout $ rsync host "eket-module.nix"
  stdout $ deploy host
