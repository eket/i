{ config, pkgs, lib, ... } :
let
  cfg = config.services.eket;
  goreModulePath = "/root/gore/infrastructure/gore.nix";
  gore =
    if lib.pathExists goreModulePath then
      {
        imports = [ goreModulePath ];
        config = {
          services.gore.serverName = "gore.${cfg.domain}";
          services.gore.enableSSL = cfg.enableSSL;
        };
      }
    else
      lib.warn ("gore NixOS module not found at " + goreModulePath + " . Skipping gore configuration.") { imports = []; config = {}; };
in {
  imports = [] ++ gore.imports;

  options.services.eket = with lib; with lib.types; {
    env = mkOption {
      type = string;
    };

    domain = mkOption {
      type = string;
    };

    enableSSL = lib.mkOption {
      type = bool;
      default = true;
    };
  };

  config = lib.mkMerge [ gore.config {
    users.users.root.openssh.authorizedKeys.keys = [
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBNCCbqb2RNOAug3a/PiSNDNZrZ01tnKe9fBr2TUM4I4M4F3ie9ifzTRsXWx2Ls3+nyEzd2fU1fr3OeGHNIZnYrM= martonboros@gmail.com"
    ];

    networking.extraHosts = "127.0.0.1 ${cfg.domain} ${cfg.env}.${cfg.domain} consul.${cfg.domain}";
    networking.firewall.allowedTCPPorts = [ 22 80 443 8080 ];

    environment.systemPackages = [ pkgs.git pkgs.htop ];

    services.nginx = {
      enable = true;
      statusPage = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = cfg.enableSSL;
      recommendedGzipSettings = true;

      virtualHosts."eket-domain" = {
        serverName = "${cfg.domain}";
        forceSSL = cfg.enableSSL;
        enableACME = cfg.enableSSL;
        locations."/".root = pkgs.writeTextDir "index.html" ''
          <h2>blorf</h2>
        '';
      };

      virtualHosts."eket-subdomain" = {
        serverName = "${cfg.env}.${cfg.domain}";
        forceSSL = cfg.enableSSL;
        enableACME = cfg.enableSSL;
        locations."/".root = pkgs.writeTextDir "index.html" ''
          <h2>blorfolol</h2>
        '';
      };

      virtualHosts."consul" = {
        serverName = "consul.${cfg.domain}";
        forceSSL = cfg.enableSSL;
        enableACME = cfg.enableSSL;
        locations."/".root = "/data/www/consul/";
      };

      virtualHosts."agoramusic" = {
        serverName = "agoramusic.hu";
        forceSSL = cfg.enableSSL;
        enableACME = cfg.enableSSL;
        extraConfig = "rewrite ^/distant$ https://www.facebook.com/events/145397749745332 redirect;";
      };
    };
  }];
}
