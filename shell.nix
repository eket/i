# nix-shell environment for provisioning infrastructure and deployment

{pkgs ? import ./nixpkgs-pinned.nix {}, ...} :
pkgs.stdenv.mkDerivation {
  name = "gore-deploy-env";
  buildInputs = [ pkgs.terraform-full ];
}
